# Prep Linux hosts

> Meant as a one-shot play to add pubkeys, add the hosts to the Ansible-controller "known_hosts"-file, and some other usefull stuff. However, due to Ansible's idempotent capabilities target hosts will remain untouched if your keys are already in place.  
> Usefull for AD-integrated Linux hosts.

## HOW TO USE:
- Log into an Ansible controller host via ssh, using your *AD*-user
- Optionally, create a directory and move into it (`$ mkdir <dir> && cd <dir>`)
- Clone the "addmykeys" git project:  
`$ git clone https://gitlab.com/Binker/addmykeys.git`  
and enter the password for \<your_gitlab_user\>
- Move into the "addmykeys" directory (`$ cd addmykeys`)  

### Run against target host(s) directly:
Although a dynamic inventory might be best for rolling out your credentials to a larger number of target hosts, you can choose to supply the Play with a single target, or maybe just a few targets, on the command line.  
Run like this:  
`$ ansible-playbook -k -K -i <fqdn1>,<fqdn2>,<etc...>, addmykeys.yml`  

### Supply your credentials:
Please note the following:  
`-k, --ask-pass` is ask for connection password on the target host.  
`-K, --ask-become-pass` is ask for privilege escalation password.  
This will be your sudo password.  

### Additional option:
Enable root login by adding the tag "give_root".  
`$ ansible-playbook -k -K addmykeys.yml -t "give_root"`   
